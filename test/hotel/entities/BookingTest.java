/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

import hotel.credit.CreditCard;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;

/**
 *
 * @author Helkern
 */
public class BookingTest {

    @Mock ServiceType serviceType;    
    @Mock Guest guest;
    @Mock int stayLength;
    @Mock int numberOfOccupants;
    @Mock CreditCard creditCard;
    Date arrivalDate = new Date(2018,10,20);
    double cost = 10.0;
    double maximumDelta = 0.1;
    Room room = new Room(stayLength, RoomType.SINGLE);
    /**
     * Test of addServiceCharge method, of class Booking.
     */
    @Test
    public void testAddServiceCharge() {
        System.out.println("addServiceCharge");
        Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);
        booking.addServiceCharge(serviceType, cost);
        assertEquals(cost, booking.getCharges().get(0).getCost(), maximumDelta);
    }

}
