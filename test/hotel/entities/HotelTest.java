/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel.entities;

import hotel.HotelHelper;
import hotel.credit.CreditCard;
import hotel.service.RecordServiceCTL;
import java.util.Date;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;

/**
 *
 * @author Helkern
 */
public class HotelTest {

    Hotel hotel;
    @Mock Guest guest;
    @Mock int stayLength;
    @Mock int numberOfOccupants;
    @Mock CreditCard creditCard;
    Date arrivalDate = new Date(2018,10,20);

    Room room = new Room(101, RoomType.DOUBLE);

    /**
     * Test of checkout method, of class Hotel.
     */
    @Test
    public void testCheckout() throws Exception {
        hotel = HotelHelper.loadHotel();
        Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);
        hotel.activeBookingsByRoomId.put(room.id, booking);
        System.out.println("checkout");
        hotel.checkout(room.id);
        // findActiveBookingByRoomId must return null, when checkout completed for the same room.
        booking = hotel.findActiveBookingByRoomId(room.id);
        assertNull("findActiveBookingByRoomId must return null, when checkout completed for the same room",booking);
    }

}
